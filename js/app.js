/* global lib */

var BASEURL = 'http://api.themoviedb.org/3/discover/movie?api_key=889c94824f4d141b02f9caf2d4b58f44';

// A menu class which takes care of navigation like left right keys.
var Menu = function (config) {
	this.view = config.view;
	this.current = null;
	var instance = this;
	this.view.addEventListener("keyup", function (e) {
		if (e.keyCode == '40') {
			// down arrow
			document.getElementById('view').firstChild.focus(); //Making thumbnail container focused.
		} else if (e.keyCode == '37') {
			// left arrow
			var prev = instance.current;
			// This will be better option than taking children of parent node and fetching previous/next based on index.
			while(true) {
				var prev = prev.previousSibling;
				if (prev === null) {
					break;
				} else if (prev.tagName === 'A') { // Strangly for anchor tags, text comes when traversing through siblings.
					location.hash = prev.hash;
					break;
				}
			}
		} else if (e.keyCode == '39') {
			// right arrow
			var next = instance.current;
                        // This will be better option than taking children of parent node and fetching previous/next based on index.
			while(true) {
				var next = next.nextSibling;
				if (next === null) {
					break;
				} else if (next.tagName === 'A') {
					location.hash = next.hash;
					break;
				}
			}
		}
	});
};

// Will be called by thumbnail controllers to highlight selected menu item.
Menu.prototype.focus = function (name) {
	var menuitem = this.view.getElementsByClassName(name)[0];
	if (menuitem) {
		lib.removeClass(this.current, 'focus');
		lib.addClass(menuitem, 'focus');
		this.current = menuitem;
	}
};


/*
 * Base class for all thumbnail controllers.
 */
var ThumbnailController = function () {
	this.url = null;//Should be set by subclass instance.
	this.columns = 2;//Number of items have to dispalyed per row. (Ensure CSS is set accordingly).
	this.rows = 3;//Number of rows per page.
	this.viewport_items = this.columns * this.rows;//Number of items per page.
	this.page = 0; //Current page.
	this.data = null;//Data recieved from server.
	this.element = null;//The thumbnails container element.
	this.current = null;//Currently focused element.
	this.current_index = null;//Currently focused element index.
};

//These key fucntions will be fired when up/down/left/right arrow keys pressed.
ThumbnailController.prototype.keyup = function () {
	var prev = this.current;
        //Fetching children and then move by index will be slower in case number of items per page will be more.
        //Hence going with sibling travesal.
	for (var i = 0; i < this.columns; i++) {
		prev = prev.previousSibling;
		if (prev === null) { // Ah reached first row, lets go to menu.
			document.getElementById('menu').focus();
			return;
		}
	}
	this.current_index -= this.columns;
	var current = this.current;
	this.blurItemElement(current);
	this.focusItemElement(prev);
};

ThumbnailController.prototype.keydown = function () {
	var next = this.current;
	for (var i=0; i<this.columns; i++) {
		next = next.nextSibling;
		// Ah reached last row, lets not do anything.
		// Probably we could go to the next page if required.
		if (next === null) {
			return;
		}
	}
	this.current_index += this.columns;
	var current = this.current;
	this.blurItemElement(current);
	this.focusItemElement(next);
};

ThumbnailController.prototype.keyleft = function () {
	if ((this.current_index) % this.columns === 0) { // Am i at the left corner
		if (this.page > 0) { // If it's not a first page, lets go to prev page.
			// How about focusing on prev element if I am not at the top row ?
			this.page --;
			this.renderPage();
		}
	} else { // Move left
		this.current_index --;
		var current = this.current;
		this.blurItemElement(current);
		this.focusItemElement(current.previousSibling);
	}
};

ThumbnailController.prototype.keyright = function () {
	if ((this.current_index + 1) % this.columns === 0) { // Am i at the right corner
		// How about focusing on next element if I am not at the bottom row ?
		this.page ++;
		this.renderPage();
	} else { //Move right
		this.current_index ++;
		var current = this.current;
		this.blurItemElement(current);
		this.focusItemElement(current.nextSibling);
	}
};

//This function will be invoked by application when hash URL changes.
ThumbnailController.prototype.render = function (element) {

	var instance = this;
	instance.element = element.firstChild;

	instance.element.addEventListener("focus", function() {
		instance.current_index = 0;
		instance.focusItemElement(instance.element.firstChild);
	});

	instance.element.addEventListener("blur", function() {
		instance.current_index = 0;
		instance.blurItemElement(instance.current);
	});

	instance.element.addEventListener("keyup", function (e) {
		if (e.keyCode == '38') {
		// up arrow
			instance.keyup();
		} else if (e.keyCode == '40') {
		// down arrow
		instance.keydown();
		} else if (e.keyCode == '37') {
		// left arrow
		instance.keyleft();
		} else if (e.keyCode == '39') {
		// right arrow
		instance.keyright();
		}
	});

	// Fetch data from service
	lib.jsonrequest(this.url, function (err, data) {
		instance.data = data;
		instance.renderPage();
	});
};

//Sets focus to the given element.
ThumbnailController.prototype.focusItemElement = function (element) {
	element.setAttribute('class', element.getAttribute('class') + ' focus');
	this.current = element;
};

ThumbnailController.prototype.blurItemElement = function (element) {
	this.current = null;
	var current_classes = element.getAttribute('class');
	element.setAttribute('class', current_classes.replace(new RegExp('focus', 'g'), ''));
};

//Creates an item element for given item.
//This function can be replaced with template engine like mustache.
ThumbnailController.prototype.getItemElement = function (item, index) {
	var item_elem = document.createElement('div');
	item_elem.setAttribute('class', 'thumbnail');
	item_elem.innerHTML = '<div class="thumbnail-inner">'
		+'<img src="https://image.tmdb.org/t/p/w370/' + item.poster_path + '"></img>'
		+'<div class="img_content">'
		+'<span>' + item.original_title + '</span>'
		+'&nbsp;<b><span class="rating">' + item.vote_average + '</b></span>'
		+'<p class="overview">' + item.overview + '</p>'
		+'</div>'
		+'</div>'
	;
	return item_elem;
};

//Render page based on the current page number.
ThumbnailController.prototype.renderPage = function () {
	console.log('renderPage');
	var instance = this;
	var data = this.data.results.slice(this.page * this.viewport_items, this.page * this.viewport_items + this.viewport_items);

	// No more elements ?, do
	if (data.length > 0) {
		// Clear pervious page items.
		instance.element.innerHTML = '';
		data.forEach(function (item, index) {
			var item_elem = instance.getItemElement(item, index);
			instance.element.appendChild(item_elem);
		});
		instance.current_index = 0;
		instance.focusItemElement(instance.element.firstChild);
	} else {
		//Lets set page to the previous
		instance.page --;
	}
};


/*
	Every category of thumbnails can have thier own customizations,
	hence subclassing ThumbnailController and overring required functions.
 */

var PopularThumbnailController = function (config) {
	ThumbnailController.call(this, config);
	this.menu = config.menu;
	this.url = BASEURL + '&sort_by=popularity.desc';
};

PopularThumbnailController.prototype = Object.create(ThumbnailController.prototype);
PopularThumbnailController.prototype.constructor = PopularThumbnailController;
PopularThumbnailController.prototype.render = function(element) {
	this.menu.focus('popular');
	ThumbnailController.prototype.render.call(this, element);
};


var TopRatedThumbnailController = function (config) {
	ThumbnailController.call(this, config);
	this.menu = config.menu;
	this.url = BASEURL + '&sort_by=vote_average.desc';
};

TopRatedThumbnailController.prototype = Object.create(ThumbnailController.prototype);
TopRatedThumbnailController.prototype.constructor = PopularThumbnailController;
TopRatedThumbnailController.prototype.render = function(element) {
	this.menu.focus('toprated');
	ThumbnailController.prototype.render.call(this, element);
};


var UpcomingThumbnailController = function (config) {
	ThumbnailController.call(this, config);
	this.menu = config.menu;
	this.url = BASEURL + '&primary_release_date.gte=2014-09-15&primary_release_date.lte=2014-10-22';
};

UpcomingThumbnailController.prototype = Object.create(ThumbnailController.prototype);
UpcomingThumbnailController.prototype.constructor = PopularThumbnailController;
UpcomingThumbnailController.prototype.render = function(element) {
	this.menu.focus('upcoming');
	ThumbnailController.prototype.render.call(this, element);
};


var NowPlayingThumbnailController = function (config) {
	ThumbnailController.call(this, config);
	this.menu = config.menu;
	this.url = BASEURL + '&primary_release_date.gte=2014-09-15&primary_release_date.lte=2014-10-22';
};

NowPlayingThumbnailController.prototype = Object.create(ThumbnailController.prototype);
NowPlayingThumbnailController.prototype.constructor = PopularThumbnailController;
NowPlayingThumbnailController.prototype.render = function(element) {
	this.menu.focus('nowplaying');
	ThumbnailController.prototype.render.call(this, element);
};

var app = new lib.Application({
	view: document.getElementById('view'),
});

var defaultRouter = new lib.Router({
	'#popular': {
		template: 'views/popular.html',
		controller : 'popularController',
	},
	'#toprated': {
		template: 'views/toprated.html',
		controller : 'topratedController',
	},
	'#upcoming': {
		template: 'views/upcoming.html',
		controller : 'upcomingController',
	},
	'#nowplaying': {
		template: 'views/nowplaying.html',
		controller : 'nowplayingController',
	},
});

app.router(defaultRouter);

//Creating a menu instance.
var menu = new Menu({
	view: document.getElementById('menu'),
});

app.controller('popularController', new PopularThumbnailController({
	menu: menu,
}));

app.controller('topratedController', new TopRatedThumbnailController({
	menu: menu,
}));

app.controller('upcomingController', new UpcomingThumbnailController({
	menu: menu,
}));

app.controller('nowplayingController', new NowPlayingThumbnailController({
	menu: menu,
}));

app.start();
