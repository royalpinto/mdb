/*
 *  This is just a nano mvc design created to demonstraste/try The Movie Database Demo.
 *  I have added basics features of MVC for demo, more features can be added based on the requirements.
 *
 *  Note:
 *  Most of the functions are prototype based, (instance has be created and then use.)
 *  These will be easier to manage when code grows bigger. also easier to subclass or overide functions.
 */

var lib = {

    //An Application Class.
    Application: (function () {

        var Application = function (config) {
            this.config = config; //Configurations.
            this.controllers = {}; //HashMap of controllers.
            this.routers = [];
        };

        /*
         * This will register controller for the application.
         * An application can have multiple controllers but name should be unique.
         */
        Application.prototype.controller = function (name, controller) {
            this.controllers[name] = controller; // Stores controller against the name.
        };

        /*
         * This will register router for the application.
         */
        Application.prototype.router = function (router) {
            router.application = this; //Setting app instance for router.
            this.routers.push(router);
        };

        /*
         * This will be invoked by the routers when there is change in hash URL.
         * This will clear the elements inside the view and invoke respective controller.
         */
        Application.prototype.showController = function (templateName, controllerName) {
            var instance = this;
            // Fetching template
            lib.request(templateName, function (err, data) {
                // Setting template data as inner HTML.
                instance.config.view.innerHTML = data;
                //Invoke controller's render method.
                instance.controllers[controllerName].render(instance.config.view);
            });
        };

        /*
         * The start function
         */
        Application.prototype.start = function () {
            this.routers.forEach(function(router) {
                router.hashchange();
            });
        };

        return Application;

    })(),

    //A Router Class.
    Router: (function () {

        var Router = function (config) {

            this.config = config;

            //Application instance will be set by application when router is registered
            this.application = null;

            var instance = this;

            this.onhaschange = function () {
                var hashconfig = instance.config[window.location.hash];
                if (hashconfig !== undefined) {
                    instance.application.showController(hashconfig['template'], hashconfig['controller']);
                }
            };
            window.addEventListener("hashchange", this.onhaschange);
        };

        // This is usefull if incase hash config has to be added or modified later.
        // Not used in this demo.
        Router.prototype.when = function (url, hashconfig) {
            this.config[url] = hashconfig; //Add or update new config.
            return this; // to support chain.
        };

        Router.prototype.hashchange = function () {
            this.onhaschange();
        };

        return Router;

    })(),

};

/*
 * All below are util methods used since its a Vanilla JavaScript.
 * Probably we can move these to a utility js file.
 */

lib.request = function(url, callback) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            callback(null, xmlhttp.responseText);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    return xmlhttp;
};


//Makes request and parse data into JSON format.
lib.jsonrequest = function(url, callback) {
    return lib.request(url, function (err, data) {
        if (err) {
            callback(err)
        } else {
            callback(err, JSON.parse(data));
        }
    });
};


lib.addClass = function (element, classname) {
    if (!element) {
        return;
    }
    var current_classes = element.getAttribute('class') || '';
    element.setAttribute('class',  current_classes + ' ' + classname);
};


lib.removeClass = function (element, classname) {
    if (!element) {
        return;
    }
    var current_classes = element.getAttribute('class') || '';
    element.setAttribute('class', current_classes.replace(new RegExp('focus', 'g'), ''));
};
